---
title: "[sitadel] projet pl4notconj"
editor_options: 
  markdown: 
    wrap: 72
---

![pel4notconj-logo](./man/figures/logo.png){width="250px"}

# Contexte

Le fichier pel4notconf résulte d'échanges initiés courant le mois de xxx
par Jean-François Colin avec Laurent Beltran, car le permier qui avait
bénéficié d'un script réalisé par Caroline Coudrin de la Deal de la
Réunion souhaitait se rapprocher du second pour envisager la meilleure
approche.

Le groupe de travail régulier Antilles-Guyane s'est réuni en visio le
vendredi 8 avril pour étudier le code initial et le paramétriser pour
ses propres besoins.

Le résultat de la transformation du script initial dénommé
`note_conj_T4_2021.Rmd` et transcrit désormais dans le fichier
`pel4notconj.Rmd`.

# Nom du script / projet

Le nom du script provient d'un acronyme capilotracté tiré de l'objet
suivant : Préparater des ELéments pour (for-\>4) la NOTe de CONJoncture
(pel4notconj) :-))

# Finalités du script

Pour l'heure produire 3 synthèse de données dont deux tableaux et un
graphique.

-   tab1: derniers chiffres logements pour la région ciblée rapportée à
    France entière
-   fig2: trois chroniques superposées (en cumul sur 12 mois) présentant
    les deux premières les logements autorisés (AUT)selon qu'ils sont
    collectifs ou individuels et la dernière les logements commencés
    (COM) le tout pour la région cible (autrement dit le DOM)
-   tab3: derniers chiffres surface de plancher pour le dom comparé à la
    France entière

# Usage

Pour s'exécuter le script s'appuie sur deux répertoires `Data`
(obligatoire) et `Sortie` (facultatif) qui doivent être dans le même
dossier que son lieu d'exécution.

Le script en effet consiste à manipuler des données qu'il doit trouver
dans le répertoire `Data`. (se reporter au point spécifique pour en
savoir plus)

Le script accepte un certain nombre d'options permettant de moduler son
comportement, elles se choisissent toutes depuis l'entête Yaml du
fichier.

``` yaml
  cogreg: '02'
  libdom: 'La Martinique'
  fichier_donnees_annee: 2021
  fichier_donnees_mois: 1
  presentation_a_fin_annee: 2020
  presentation_a_fin_mois:  12
  chiffres_arrondis_dizaine: FALSE
  alimenter_repertoire_sorties: FALSE
  marqueurfichier: ''
```

# Alimenter correctement le répertoire `Data`

Tous les mois Francois Bouton diffuse deux mails, respectivement titrés
:

-   Construction de logements (Sitadel) : estimations en date réelle à
    fin février 2022 (données sous embargo)
-   Stats Sitadel surfaces de plancher collectées (dont surfaces de
    locaux) à fin février 2022 (données DPC et DR sous embargo)

## Le mail "Construction de logements"

Ce mail contient un zip nommé selon le mois de manière régulière :
`Fichiers construction logements pour ROES_202202.7z` cette archive
contient 3 fichiers :

-   ROES_202202.xls
-   SDP_AUT_ROeS_202202.xls
-   SDP_COM_ROES_202202.xls

## Le mail "Stats Sitadel surfaces de plancher"

Ce mail renvoi vers un téléchargement melanissimo, il permet de
récuperer un archive nommée selon une codification rigoureuse :
`Surfaces de plancher collectées Sitadel dont locaux_202202.7z`.

Ce archive contient les fichiers suivants : \*
DPC_mod_AUT_202202_new_reg.xls \* DPC_mod_COM_202202_new_reg.xls \*
DR_mod_AUT_202202_new_reg.xls \* DR_mod_COM_202202_new_reg.xls

## Au final il y a donc 7 fichiers à déposer dans le répertoire `Data`

-   ROES_AAAAMM.xls
-   SDP_AUT_ROeS_AAAAMM.xls
-   SDP_COM_ROES_AAAAMM.xls
-   DPC_mod_AUT_AAAAMM_new_reg.xls
-   DPC_mod_COM_AAAAMM_new_reg.xls
-   DR_mod_AUT_AAAAMM_new_reg.xls
-   DR_mod_COM_AAAAMM_new_reg.xls

Le codification rigoureuse de ces fichiers par un marqueur AAAAMM
composé par les 4 chiffres de l'année et les deux du mois permet le
couple principal de paramètres à renseigner dans le script (outre le bon
ciblage de la région) :

``` yaml
  fichier_donnees_annee: AAAA
  fichier_donnees_mois: MM
```

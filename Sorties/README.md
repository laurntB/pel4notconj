Deux couples de 6 fichiers (au total 12) attendus si la génération des chroniques est invoquée.

Le répertoire contiendra donc des fichiers respectants les conventions d'écriture données en suivant.



# Export des donnees pour les series longues

  * Sorties/cumul_12mois_logements_autorises_{params$marqueurfichier}.csv 
  * Sorties/cumul_12mois_surf_logt_autorises_{params$marqueurfichier}.csv 
  * Sorties/cumul_12mois_logements_commences_{params$marqueurfichier}.csv 
  * Sorties/cumul_12mois_surf_logt_commences_{params$marqueurfichier}.csv 
  * Sorties/cumul_12mois_surface_plancher_autorisee_{params$marqueurfichier}.csv 
  * Sorties/cumul_12mois_surface_plancher_commencee_{params$marqueurfichier}.csv 


# Export des donnees pour les series longues (format long)

  * Sorties/cumul_12mois_logements_autorises_CERBTP_{params$marqueurfichier}.csv 
  * Sorties/cumul_12mois_surf_logt_autorises_CERBTP_{params$marqueurfichier}.csv 
  * Sorties/cumul_12mois_logements_commences_CERBTP_{params$marqueurfichier}.csv 
  * Sorties/cumul_12mois_surf_logt_commences_CERBTP_{params$marqueurfichier}.csv 
  * Sorties/cumul_12mois_surface_plancher_autorisee_CERBTP_{params$marqueurfichier}.csv 
  * Sorties/cumul_12mois_surface_plancher_commencee_CERBTP_{params$marqueurfichier}.csv 


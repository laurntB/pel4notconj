#' Graphique sur les locaux en base 100
#'
#' @param pch chroniques préparées
#' @param params paramètres globaux notamment pour alimenter diverses informations sur le graphique 
#' @param annee_reference année de référence
#' @param debut_chronique début de la chronique
#'
#' @return un graphique
#' 
#' @export
#'
ogrh_locaux <- function(pch, params, annee_reference = 2019, debut_chronique = 2013)
{
  
  libdom <- get_libelle(params$cogreg)
  date_extract <-  get_date_extract(params$fichier_donnees_annee,params$fichier_donnees_mois)
  
  base100_mq_aut <- pch$DPC_DOM_complet %>% 
    base100_factory(SDP_total_locaux_AUT_cumul12_arr, annee_reference)
  
  base100_mq_com <- pch$DPC_DOM_complet %>% 
    base100_factory(SDP_total_locaux_COM_cumul12_arr, annee_reference)
  
  data_mq <- pch$DPC_DOM_complet %>% 
    dplyr::filter(lubridate::year(date)>debut_chronique) %>%   ## , month(date)==1  
    dplyr::mutate(
      aut = base100_mq_aut(SDP_total_locaux_AUT_cumul12_arr) ,
      com = base100_mq_com(SDP_total_locaux_COM_cumul12_arr) ,
      zone = "mq"
    ) %>% 
    tidyr::pivot_longer(cols=c("aut", "com"), names_to="type", values_to="valeur") %>% 
    dplyr::select(date, zone, type, valeur)
  
  # data_mq %>% 
  #   ggplot() + 
  #   geom_line(mapping=aes(x=date, y=valeur, color=type))+
  #   scale_x_date(date_breaks = "1 year", date_labels = "%Y")
  
  
  
  base100_fr_aut <- pch$DPC_FR_complet %>% 
    base100_factory(SDP_total_locaux_AUT_cumul12_arr, annee_reference)
  
  base100_fr_com <- pch$DPC_FR_complet %>% 
    base100_factory(SDP_total_locaux_COM_cumul12_arr, annee_reference)
  
  data_fr <- pch$DPC_FR_complet %>%
    # dplyr::filter(lubridate::year(date)>2012) %>%   ## , month(date)==1 
    dplyr::mutate(
      aut = base100_fr_aut(SDP_total_locaux_AUT_cumul12_arr),
      com = base100_fr_com(SDP_total_locaux_COM_cumul12_arr),
      zone = "fr"
    ) %>% 
    tidyr::pivot_longer(cols=c("aut", "com"), names_to="type", values_to="valeur") %>% 
    dplyr::select(date, zone, type, valeur)
  
  # data_fr %>% 
  #   dplyr::filter(lubridate::year(date)>debut_chronique) %>% 
  #   ggplot() + 
  #   geom_line(aes(x=date, y=valeur, color=type) )+
  #   scale_x_date(date_breaks = "1 year", date_labels = "%Y")
  
  
  #### global
  
  g <- data_mq %>% 
    dplyr::filter(lubridate::year(date)>debut_chronique) %>%
    ggplot2::ggplot() + 
    ggplot2::geom_line(
      mapping=ggplot2::aes(x=date, y=valeur, color=zone,  linetype=type),
      size = 1.5
    ) +
    ggplot2::geom_line(
      data_fr %>% dplyr::filter(lubridate::year(date)>debut_chronique), 
      mapping=ggplot2::aes(x=date, y=valeur, color=zone,  linetype=type),
      size = 1.1
    )+    ###  
    ## scale_x_date(date_breaks = "1 year", date_labels = "%Y")
    ggplot2::labs(
      x = NULL, 
      y =glue::glue("Surface en base 100\nannée de référence {annee_reference}"),
      title = glue::glue("Surfaces de plancher des locaux autorisés et commencés à {libdom} (cumul sur 12 mois)"),
      caption =glue::glue("Source: SDES-Deal, Sit@del2, dates de prise en compte à fin {date_extract}."),
      colour= "L\u00E9gende",
      linetype = "L\u00E9gende"
      
    ) +
    ggplot2::scale_x_date(
      date_breaks = "3 months",
      #date_minor_breaks = "1 month",
      minor_breaks = "1 month",
      #limits = c(dt_x_an_avant-months(1),periode_fin_dt+months(1)),
      #limits = c(dt_x_an_avant,periode_fin_dt),
      date_labels = "%b %y"
    ) +
    # gouvdown::scale_fill_gouv_discrete(palette = "pal_gouv_div1", reverse = TRUE) +
    # scale_color_manual(values = c("black")) +
    ggplot2::guides(
      fill=ggplot2::guide_legend(title = NULL, nrow=2), 
      colour=ggplot2::guide_legend(title = NULL, nrow=1),
      alpha="none") +
    ggplot2::theme(  
      legend.position = "bottom", 
      #legend.justification = c("right", "top"),
      legend.box.just = "right",
      legend.margin = ggplot2::margin(2, 6, 6, 6),
      panel.background = ggplot2::element_rect(fill = "white"),
      panel.grid.major.x = ggplot2::element_line(colour = "grey90"),
      panel.grid.minor.x = ggplot2::element_line(colour = "grey90"),
      panel.grid.major.y = ggplot2::element_line(colour = "grey90"),
      plot.caption = ggplot2::element_text(hjust = 0, face= "italic"),
      plot.title.position = "plot",
      plot.caption.position =  "plot",
      axis.text.x = ggplot2::element_text(angle = 45, hjust = 1) 
    )
  
  ## https://stackoverflow.com/questions/37140266/how-to-merge-color-line-style-and-shape-legends-in-ggplot
  ## g <- g + guides(colour = guide_legend(override.aes = list(linetype = override.linetype)))
  ## g + scale_linetype(guide = FALSE)
  
  g
}
